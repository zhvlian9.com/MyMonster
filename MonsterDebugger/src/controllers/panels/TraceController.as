package controllers.panels
{
	import com.demonsters.debugger.MonsterDebuggerConstants;
	import com.demonsters.debugger.MonsterDebuggerUtils;
	
	import components.Filter;
	import components.panels.TracePanel;
	import components.windows.SnapshotWindow;
	import components.windows.TraceWindow;
	
	import flash.display.BitmapData;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.filesystem.*;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	
	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.DataGridEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.events.ScrollEvent;
	import mx.events.StateChangeEvent;
	import mx.utils.StringUtil;
	
	import org.osmf.events.DisplayObjectEvent;


	public final class TraceController extends EventDispatcher
	{

		[Bindable]
		private var _dataFilterd:ArrayCollection = new ArrayCollection();
		
		private var _data:ArrayCollection = new ArrayCollection();
		private var _panel:TracePanel;
		private var _send:Function;
		private var _selectedItem:Object;
		private var _maxLines:int = Number.NaN;
		
		/**
		 * Save panel
		 */
		public function TraceController(panel:TracePanel, send:Function)
		{
			_panel = panel;
			_send = send;
			_panel.addEventListener(FlexEvent.CREATION_COMPLETE, creationComplete, false, 0, true);
		}


		/**
		 * Panel is ready to link data providers
		 */
		private function creationComplete(e:FlexEvent):void
		{
			_panel.removeEventListener(FlexEvent.CREATION_COMPLETE, creationComplete);
			_panel.datagrid.dataProvider = _dataFilterd;
			_panel.datagrid.addEventListener(ListEvent.ITEM_DOUBLE_CLICK, showTrace, false, 0, true);
			_panel.datagrid.addEventListener(ListEvent.ITEM_CLICK, clickRow, false, 0, true);
			_panel.filter.addEventListener(Filter.CHANGED, filterChanged, false, 0, true);
			_panel.clearButton.addEventListener(MouseEvent.CLICK, clear, false, 0, true);
			_panel.exportLogButton.addEventListener(MouseEvent.CLICK, exportLog,false, 0, true);
			_panel.chkRegExp.addEventListener(MouseEvent.CLICK, filterChanged, false, 0, true);
			_panel.selectedRowButton.addEventListener(MouseEvent.CLICK, selectedRow, false, 0, true);
			_panel.btnSetMaxLines.addEventListener(MouseEvent.CLICK, maxLines, false, 0, true);
		}

		
		private function clickRow(event:ListEvent):void
		{
//			if( _panel.datagrid.selectedItem == _selectedItem)
//			{
//				_panel.datagrid.selectedItem = null;
//			}
			_selectedItem = _panel.datagrid.selectedItem;
		}
		
		
		/**
		 * Show a trace in an output window
		 */
		private function filterChanged(event:Event):void
		{
			_dataFilterd.removeAll();
			
			// Check if a filter term is given
			if (_panel.filter.words.length == 0) {
				for (var i:int = 0; i < _data.length; i++) {
					_dataFilterd.addItem(_data[i]);
				}
			}
			else{
				for (var n:int = 0; n < _data.length; n++) {
					if (checkFilter(_data[n])) {
						_dataFilterd.addItem(_data[n]);
					}
				}
			}
		}
		
		private function selectedRow(event:MouseEvent):void
		{
			_panel.datagrid.selectedItem = _selectedItem;
			if(_panel.datagrid.selectedIndex != -1)
			{
				_panel.datagrid.verticalScrollPosition = _panel.datagrid.selectedIndex;
			}
		}
		
		private function addItem(item:Object):void
		{
			if (_panel.filter.words.length == 0) {
				_dataFilterd.addItem(item);
				return;
			}
			if (checkFilter(item)) {
				_dataFilterd.addItem(item);
			}
		}

		
		private function checkFilter(item:Object):Boolean
		{
			if(_panel.chkRegExp.selected)
			{
				var reg:RegExp = new RegExp(_panel.filter.label.text, "i");
				return checkFilterReg(item, reg);
			}
			else
			{
				return checkFilterLoop(item);
			}
		}
		
		private function checkFilterReg(item:Object, pattern:RegExp):Boolean
		{
			var message:String = item.message;
			var target:String = item.target;
			var label:String = item.label;
			var person:String = item.person;
			if (message == null) message = "";
			if (target == null) target = "";
			if (label == null) label = "";
			if (person == null) person = "";
			message = StringUtil.trim(message).toLowerCase();
			target = StringUtil.trim(target).toLowerCase();
			label = StringUtil.trim(label).toLowerCase();
			person = StringUtil.trim(person).toLowerCase();
			
			if(message.search(pattern) != -1)	return true;
			if(target.search(pattern) != -1)	return true;
			if(label.search(pattern) != -1)	 return true;
			if(person.search(pattern) != -1)	return true;
			return false;
		}		
		
		/**
		 * Loop through the search terms and compare strings
		 */
		private function checkFilterLoop(item:Object):Boolean
		{
			var message:String = item.message;
			var target:String = item.target;
			var label:String = item.label;
			var person:String = item.person;
			if (message == null) message = "";
			if (target == null) target = "";
			if (label == null) label = "";
			if (person == null) person = "";
			message = StringUtil.trim(message).toLowerCase();
			target = StringUtil.trim(target).toLowerCase();
			label = StringUtil.trim(label).toLowerCase();
			person = StringUtil.trim(person).toLowerCase();
			var i:int;
			
			// Clone words
			var words:Array = [];
			for (i = 0; i < _panel.filter.words.length; i++) {
				words[i] = _panel.filter.words[i];
			}
			
			if (message != "") {
				for (i = 0; i < words.length; i++) {
					if (message.indexOf(words[i]) != -1) {
						words.splice(i, 1);
						i--;
					}
				}
			}
			if (words.length == 0) return true;
			if (target != "") {
				for (i = 0; i < words.length; i++) {
					if (target.indexOf(words[i]) != -1) {
						words.splice(i, 1);
						i--;
					}
				}
			}
			if (words.length == 0) return true;
			if (label != "") {
				for (i = 0; i < words.length; i++) {
					if (label.indexOf(words[i]) != -1) {
						words.splice(i, 1);
						i--;
					}
				}
			}
			if (words.length == 0) return true;
			if (person != "") {
				for (i = 0; i < words.length; i++) {
					if (person.indexOf(words[i]) != -1) {
						words.splice(i, 1);
						i--;
					}
				}
			}
			if (words.length == 0) return true;
			return false;
		}
		
		/**
		 * Show a trace in an output window
		 */
		private function showTrace(event:ListEvent):void
		{
			// Check if the selected item is still available
			if (event.currentTarget.selectedItem != null) {
				
				// Get the data
				var item:Object = _dataFilterd.getItemAt(event.currentTarget.selectedIndex);

				// Check the window to open
				if (item.message == "Snapshot" && item.xml == null) {
					var snapshotWindow:SnapshotWindow = new SnapshotWindow();
					snapshotWindow.setData(item);
					snapshotWindow.open();
				} else {
					var traceWindow:TraceWindow = new TraceWindow();
					traceWindow.setData(item);
					traceWindow.open();
				}
			}
		}

		
		/**
		 * Clear traces
		 */
		public function clear(event:MouseEvent = null):void
		{
			_data.removeAll();
			_dataFilterd.removeAll();
			_panel.datagrid.horizontalScrollPosition = 0;
		}
		
		private function exportLog(event:MouseEvent = null):void
		{
			var docsDir:File = File.documentsDirectory;
			try
			{
				docsDir.browseForSave("Save As");
				docsDir.addEventListener(Event.SELECT, function(event:Event):void{
					var newFile:File = event.target as File;
					var str:String = new String();
					for(var i:int=0;i<_data.length;i++)
					{
						var xml:* = _data[i].xml;
						var t:String = xml.node.@type;
						var l:int = xml..node.length();
						if (l == 2 && t == MonsterDebuggerConstants.TYPE_NULL || t == MonsterDebuggerConstants.TYPE_STRING || t == MonsterDebuggerConstants.TYPE_BOOLEAN || t == MonsterDebuggerConstants.TYPE_NUMBER || t == MonsterDebuggerConstants.TYPE_INT || t == MonsterDebuggerConstants.TYPE_UINT)
						{
							// Set the textfield
							str += _data[i].time+"\t"+_data[i].target+"\t"+_data[i].reference+"\t"+MonsterDebuggerUtils.htmlUnescape(xml..node[1].@value)+"\r\n";
						}
					}
					var stream:FileStream = new FileStream();
					stream.open(newFile, FileMode.WRITE);
					stream.writeUTFBytes(str);
					stream.close();
				});
			}
			catch (error:Error)
			{
				trace("Failed:", error.message);
			}
		}

		private function maxLines(event:MouseEvent):void
		{
			_maxLines = parseInt(StringUtil.trim(_panel.txtMaxLines.text));
			removeRedundantData();
		}
		
		private function removeRedundantData():void
		{
			//keep the first log
			if(!isNaN(_maxLines) && _maxLines > 0 && _data.length > _maxLines && _data.length > 1)
			{
				for(var i:int=0;i<_data.length-_maxLines;i++)
				{
					if(_dataFilterd.contains(_data[1]))
					{
						_dataFilterd.removeItemAt(1);
					}
					_data.removeItemAt(1);
				}
			}
		}
		
		/**
		 * Data handler from client
		 */
		public function setData(data:Object):void
		{
			removeRedundantData();
			// Vars for loop
			var date:Date;
			var hours:String;
			var minutes:String;
			var seconds:String;
			var miliseconds:String;
			var time:String;
			var memory:String;
			var obj:Object;

			switch (data["command"]) {
				
				case MonsterDebuggerConstants.COMMAND_CLEAR_TRACES:
					clear();
					break;
					
				case MonsterDebuggerConstants.COMMAND_TRACE:
					
					// Format the properties
					date = data["date"];
					hours = (date.getHours() < 10) ? "0" + date.getHours().toString() : date.getHours().toString();
					minutes = (date.getMinutes() < 10) ? "0" + date.getMinutes().toString() : date.getMinutes().toString();
					seconds = (date.getSeconds() < 10) ? "0" + date.getSeconds().toString() : date.getSeconds().toString();
					miliseconds = date.getMilliseconds().toString();
					time = hours + ":" + minutes + ":" + seconds + "." + miliseconds;
					memory = Math.round(data["memory"] / 1024) + " Kb";
					
					// Create the trace object
					obj = {};
					
					// Check the label
					if (data["xml"]..node.length() > 1 && data["xml"]..node.length() <= 3) {
						if (data["xml"].node[0].@type == MonsterDebuggerConstants.TYPE_STRING || data["xml"].node[0].@type == MonsterDebuggerConstants.TYPE_BOOLEAN || data["xml"].node[0].@type == MonsterDebuggerConstants.TYPE_NUMBER || data["xml"].node[0].@type == MonsterDebuggerConstants.TYPE_INT || data["xml"].node[0].@type == MonsterDebuggerConstants.TYPE_UINT) {
							obj.message = MonsterDebuggerUtils.stripBreaks(MonsterDebuggerUtils.htmlUnescape(data["xml"].node.children()[0].@label));
						} else {
							obj.message = MonsterDebuggerUtils.stripBreaks(MonsterDebuggerUtils.htmlUnescape(data["xml"].node.@label)) + " ...";
						}
					} else {
						obj.message = MonsterDebuggerUtils.stripBreaks(MonsterDebuggerUtils.htmlUnescape(data["xml"].node.@label)) + " ...";
					}
					
					// Add extra info
					obj.line = _data.length == 0 ? 1 : _data[_data.length-1].line + 1;
					obj.time = time;
					obj.memory = memory;
					obj.reference = data["reference"];
					obj.target = data["target"];
					obj.label = data["label"];
					obj.person = data["person"];
					obj.color = data["color"];
					obj.xml = data["xml"];
					
					// Add to list
					_data.addItem(obj);
					addItem(obj);
					break;
					
				case MonsterDebuggerConstants.COMMAND_SNAPSHOT:
					
					// Format the properties
					date = data["date"];
					hours = (date.getHours() < 10) ? "0" + date.getHours().toString() : date.getHours().toString();
					minutes = (date.getMinutes() < 10) ? "0" + date.getMinutes().toString() : date.getMinutes().toString();
					seconds = (date.getSeconds() < 10) ? "0" + date.getSeconds().toString() : date.getSeconds().toString();
					miliseconds = date.getMilliseconds().toString();
					time = hours + ":" + minutes + ":" + seconds + "." + miliseconds;
					memory = Math.round(data["memory"] / 1024) + " Kb";
					
					// Read the bitmap
					try {
						var bitmapData:BitmapData = new BitmapData(data["width"], data["height"]);
						bitmapData.setPixels(new Rectangle(0, 0, data["width"], data["height"]), data["bytes"]);
					} catch(e:Error) {
						return;
					}
					
					// Create the trace object
					obj = {};
					obj.line = _data.length + 1;
					obj.time = time;
					obj.memory = memory;
					obj.width = data["width"];
					obj.height = data["height"];
					obj.bitmapData = bitmapData;
					obj.message = "Snapshot";
					obj.target = data["target"];
					obj.label = data["label"];
					obj.person = data["person"];
					obj.color = data["color"];
					obj.xml = null;
					
					// Add to list
					_data.addItem(obj);
					addItem(obj);
					break;
			}
		}
	}
}
